﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.AggregatesModel.Document.Models
{
    public class Document
    {
        [PrimaryKey, AutoIncrement, NotNull] public int Doc_ID { get; set; }
        [MaxLength(3), NotNull] public string Doc_Type { get; set; }
        [MaxLength(10), Unique, NotNull] public string Doc_Code { get; set; }
        [NotNull] public DateTime Doc_Creation { get; set; }
    }
}
