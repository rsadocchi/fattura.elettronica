﻿using DDD.Filters;
using DDD.SeedWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDD.AggregatesModel
{
    public interface IAnagMasterRepository : IRepository<AnagMaster, int>
    {
        Task<IQueryable<AnagMaster_Address>> GetAddressesAsync(int anagID);
        Task<IQueryable<AnagMaster_Contact>> GetContactsAsync(int anagID);
        Task<IQueryable<AnagMaster_Role>> GetRolesAsync(int anagID);
        Task<IQueryable<AnagMaster>> GetFromFilterAsync(AnagMasterFilterModel filters);

        Task<bool> CheckRoleAsync(int? roleID = null, string role = null);

        Task<AnagMaster> AddRoleIfNotExistsAsync(int anagID, string role);
        Task<AnagMaster> AddContactAsync(AnagMaster_Contact contact);
        Task<AnagMaster> AddAddressAsync(AnagMaster_Address address);
    }
}
