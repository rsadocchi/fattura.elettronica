﻿using DDD.SeedWorks;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.AggregatesModel
{
    [Table("AnagMaster_Contact")]
    public class AnagMaster_Contact : IAggregateRoot
    {
        [PrimaryKey, AutoIncrement, NotNull] public int AngC_ID { get; set; }
        [NotNull] public int AngC_AnagID { get; set; }
        [MaxLength(100), NotNull] public string AngC_Lastname { get; set; }
        [MaxLength(100), NotNull] public string AngC_Firstname { get; set; }
        [MaxLength(30), NotNull] public string AngC_Phone { get; set; }
        [MaxLength(30), NotNull] public string AngC_Mail { get; set; }
        [NotNull] public bool IsDefault { get; set; }
        [MaxLength(200)] public string AngC_Address { get; set; }
        [MaxLength(50)] public string AngC_City { get; set; }
        [MaxLength(10)] public string AngC_PostCode { get; set; }
        [MaxLength(10)] public string AngC_CountrySpec { get; set; }
        [MaxLength(10)] public string AngC_Country { get; set; }
        

        public int GetID() { return AngC_ID; }

        public virtual AnagMaster AnagMaster { get; set; }
    }
}
