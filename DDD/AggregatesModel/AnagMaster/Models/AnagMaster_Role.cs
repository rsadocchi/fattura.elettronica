﻿using DDD.SeedWorks;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.AggregatesModel
{
    [Table("AnagMaster_Role")]
    public class AnagMaster_Role : IAggregateRoot
    {
        [PrimaryKey, AutoIncrement, NotNull] public int AngR_ID { get; set; }
        [Unique, MaxLength(5), NotNull] public string AngR_Code { get; set; }
        [NotNull] public bool AngR_ForAnag { get; set; }
        [NotNull] public bool AngR_ForContact { get; set; }
        [MaxLength(30)] public string AngR_Description { get; set; }

        public int GetID() { return AngR_ID; }

        public AnagMaster_Role()
        {
            AngR_ForAnag = true;
            AngR_ForContact = false;
        }
    }
}
