﻿using DDD.SeedWorks;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.AggregatesModel
{
    [Table("AnagMaster_AngXRole")]
    public class AnagMaster_AngXRole : IAggregateRoot
    {
        [PrimaryKey, AutoIncrement, NotNull] public int AngAXR_ID { get; set; }
        [NotNull] public int AngAXR_AnagID { get; set; }
        [NotNull] public int AngAXR_RoleID { get; set; }

        public int GetID() { return AngAXR_ID; }

        public virtual AnagMaster AnagMaster { get; set; }
    }
}
