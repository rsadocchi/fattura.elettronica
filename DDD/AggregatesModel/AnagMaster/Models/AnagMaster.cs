﻿using DDD.SeedWorks;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.AggregatesModel
{
    [Table("AnagMaster")]
    public class AnagMaster : IAggregateRoot
    {
        [PrimaryKey, AutoIncrement, NotNull] public int Ang_ID { get; set; }
        [Unique, MaxLength(30), NotNull] public string Ang_VATNo { get; set; }
        [Unique, MaxLength(30), NotNull] public string Ang_TaxCode { get; set; }
        [MaxLength(200), NotNull] public string Ang_Name { get; set; }
        [MaxLength(30)] public string Ang_Phone { get; set; }
        [MaxLength(30)] public string Ang_Mail { get; set; }
        [MaxLength(50)] public string Ang_Website { get; set; }

        public int GetID() { return Ang_ID; }

        public AnagMaster()
        {
            AnagMaster_AngXRoles = new HashSet<AnagMaster_AngXRole>();
            AnagMaster_Addresses = new HashSet<AnagMaster_Address>();
            AnagMaster_Contacts = new HashSet<AnagMaster_Contact>();
        }

        public virtual ICollection<AnagMaster_AngXRole> AnagMaster_AngXRoles { get; set; }
        public virtual ICollection<AnagMaster_Address> AnagMaster_Addresses { get; set; }
        public virtual ICollection<AnagMaster_Contact> AnagMaster_Contacts { get; set; }
    }
}
