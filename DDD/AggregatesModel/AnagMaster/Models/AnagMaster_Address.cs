﻿using DDD.SeedWorks;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.AggregatesModel
{
    [Table("AnagMaster_Address")]
    public class AnagMaster_Address : IAggregateRoot
    {
        [PrimaryKey, AutoIncrement, NotNull] public int AngA_ID { get; set; }
        [NotNull] public int AngA_AnagID { get; set; }
        [MaxLength(200), NotNull] public string AngA_Address { get; set; }
        [MaxLength(50), NotNull] public string AngA_City { get; set; }
        [MaxLength(10), NotNull] public string AngA_PostCode { get; set; }
        [MaxLength(10), NotNull] public string AngA_CountrySpec { get; set; }
        [MaxLength(10), NotNull] public string AngA_Country { get; set; }
        [NotNull] public bool IsDefault { get; set; }
        [MaxLength(30)] public string AngA_Phone { get; set; }
        [MaxLength(30)] public string AngA_Mail { get; set; }

        public int GetID() { return AngA_ID; }

        public virtual AnagMaster AnagMaster { get; set; }
    }
}
