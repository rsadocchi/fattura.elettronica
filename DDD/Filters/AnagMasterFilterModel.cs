﻿using DDD.AggregatesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Filters
{
    public class AnagMasterFilterModel
    {
        public string VATNo { get; set; } = "";
        public string TaxCode { get; set; } = "";
        public string Name { get; set; } = "";
        public string Role { get; set; } = "";
        public string ContactName { get; set; } = "";
    }
}
