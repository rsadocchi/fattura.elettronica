﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.SeedWorks
{
    public interface IAggregateRoot
    {
        int GetID();
    }
}
