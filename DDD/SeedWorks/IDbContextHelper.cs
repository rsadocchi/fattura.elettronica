﻿
namespace DDD.SeedWorks
{
    public interface IDbContextHelper
    {
        string GetLocalDbAddress(string name);
    }

    public interface IDbContextStarter
    {
        System.Threading.Tasks.Task InitializeContextAsync();
    }
}
