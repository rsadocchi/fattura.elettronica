﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.SeedWorks
{
    public interface IUnitOfWork : IDisposable
    {
        System.Threading.Tasks.Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
        //System.Threading.Tasks.Task<bool> SaveEntitiesAsync(System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken));
    }
}
