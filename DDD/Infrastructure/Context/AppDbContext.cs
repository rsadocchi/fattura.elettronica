﻿using DDD.AggregatesModel;
using DDD.SeedWorks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;

namespace DDD.Infrastructure.Context
{
    public class AppDbContext : DbContext, IUnitOfWork
    {
        public DbSet<AnagMaster> AnagMasters { get; set; }
        public DbSet<AnagMaster_Address> AnagMaster_Addresses { get; set; }
        public DbSet<AnagMaster_Contact> AnagMaster_Contacts { get; set; }

        public AppDbContext(DbContextOptions options) : base(options: options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AnagMaster>().HasMany(o => o.AnagMaster_Addresses).WithOne(o => o.AnagMaster);
            modelBuilder.Entity<AnagMaster>().HasMany(o => o.AnagMaster_Contacts).WithOne(o => o.AnagMaster);
            modelBuilder.Entity<AnagMaster>().HasMany(o => o.AnagMaster_AngXRoles).WithOne(o => o.AnagMaster);

        }
    }
}
