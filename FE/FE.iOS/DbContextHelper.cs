﻿using FE.Interfaces;
using System;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(FE.iOS.DbContextHelper))]
namespace FE.iOS
{
    public class DbContextHelper : IDbOnPlatformHelper
    {
        public string GetLocalDbAddress(string name)
        {
            var folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "..", "Library", "Databases");
            if (!Directory.Exists(folder))
            {
                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Write(ex.Message);
                    return null;
                }
            }

            var dbPath = Path.Combine(folder, name);
            if (!File.Exists(dbPath))
            {
                FileStream fs = null;
                try
                {
                    fs = File.Create(dbPath);
                    fs.Close();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Write(ex.Message);
                    return null;
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                        fs.Dispose();
                        fs = null;
                    }
                }
            }
            return dbPath;
        }
    }
}