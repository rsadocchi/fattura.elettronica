﻿using FE.Interfaces;
using System;
using System.Diagnostics;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(FE.Droid.DbContextHelper))]
namespace FE.Droid
{
    public class DbContextHelper : IDbOnPlatformHelper
    {
        public string GetLocalDbAddress(string name)
        {
            var folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Databases");
            if (!Directory.Exists(folder))
            {
                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch (Exception ex)
                {
                    Debug.Write(ex.Message);
                    return null;
                }
            }

            var dbpath = Path.Combine(folder, name);
            if (!File.Exists(dbpath))
            {
                FileStream fs = null;
                try
                {
                    fs = File.Create(dbpath);
                    fs.Close();
                }
                catch (Exception ex)
                {
                    Debug.Write(ex.Message);
                    return null;
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                        fs.Dispose();
                        fs = null;
                    }
                }
            }
            return dbpath;
        }
    }
}