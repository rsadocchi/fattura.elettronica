﻿using FE.Interfaces;
using System.IO;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(FE.UWP.DbContextHelper))]
namespace FE.UWP
{
    public class DbContextHelper : IDbOnPlatformHelper
    {
        public string GetLocalDbAddress(string name)
        {
            var dbpath = Path.Combine(ApplicationData.Current.RoamingFolder.Path, name);
            if (!File.Exists(dbpath))
            {
                FileStream stream = null;
                try
                {
                    stream = File.Create(dbpath);
                    stream.Close();
                }
                catch
                {
                    return null;
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                        stream.Dispose();
                        stream = null;
                    }
                }
            }
            return dbpath;
        }

    }
}
