﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FE.Context
{
    public class DbContext
    {
        #region PRIVATE Properties
        private readonly SQLiteAsyncConnection Context;
        private readonly bool IsDbInitialized;
        #endregion

        #region PUBLIC Properties
        public Exception Error { get; private set; }
        
        #endregion

        public DbContext(string database)
        {
            Context = new SQLiteAsyncConnection(database, false);
            IsDbInitialized = false;
        }
    }
}
