﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FE.Common
{
    public static class IconUtility
    {
        static string IconPrefix
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        return "";
                    case Device.Android:
                        return "";
                    case Device.UWP:
                        return "Images/";
                    case Device.WPF:
                        return "Images/";
                    default:
                        return "";
                }
            }
        }

        // esempio
        // public static string ArrowUp { get { return $"{IconPrefix}arrow_up.png"; } }
    }

    public static class Extensions
    {
        public static string ToCapitalize(this string word)
        {
            var tmp = word.ToLower().Split(Convert.ToChar(" "));
            for (int i = 0; i < tmp.Length; i++)
            {
                if (tmp[i].Contains("'") && tmp[1].IndexOf(Convert.ToChar("'")) < (tmp[i].Length - 1))
                {
                    tmp[i] = $"{tmp[i].Substring(0, 1).ToUpper()}'{tmp[i].Substring(2, 1).ToUpper()}{tmp[i].Substring(3)}";
                }
                else
                {
                    tmp[i] = $"{tmp[i].Substring(0, 1).ToUpper()}{tmp[i].Substring(1)}";
                }
            }
            return string.Join(" ", tmp);
        }
    }
}
